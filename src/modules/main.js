const CHIPS_SELECT = 'CHIPS_SELECT';
const DIAMOND_SELECT = 'DIAMOND_SELECT';
const ORIUM_SELECT = 'ORIUM_SELECT';
const RECEIVE_BALANCE = 'RECEIVE_BALANCE';
const LEVEL_SELECT = 'LEVEL_SELECT';
const ENTER_ROOM = 'ENTER_ROOM';
const EXIT_ROOM = 'EXIT_ROOM';
const BACK_TO_LEVEL_SELECT = 'BACK_TO_LEVEL_SELECT';
const BACK_TO_START = 'BACK_TO_START';
const RESET_MAIN = 'RESET_MAIN';
const PLAY_UNSET = 'PLAY_UNSET';
const LEVEL_UNSET = 'LEVEL_UNSET';

const initialState = {
    currentBalance: 0,
    balanceChips: 0,
    balanceDiamond: 0,
    balanceOrium: 0,
    selectPlay: null,
    selectLevel: null,
    room: "000",
}

const main = (state = initialState, action) => {
    switch (action.type) {
        case CHIPS_SELECT:
            return state = {
                ...state,
                selectPlay: "chips",
                currentBalance: state.balanceChips
            }
        case DIAMOND_SELECT:
            return state = {
                ...state,
                selectPlay: "diamond",
                currentBalance: state.balanceDiamond
            }
        case ORIUM_SELECT:
            return state = {
                ...state,
                selectPlay: "orium",
                currentBalance: state.balanceOrium
            }
        case PLAY_UNSET:
            return state = {
                ...state,
                selectPlay: null,
                currentBalance: 0
            }
        case RECEIVE_BALANCE:
            return state = {
                ...state,
                balanceChips: action.payloadChips,
                balanceDiamond: action.payloadDiamond,
                balanceOrium: action.payloadOrium
            }
        case LEVEL_SELECT:
            return state = {
                ...state,
                selectLevel: action.payloadLvl,
            }
        case LEVEL_UNSET:
            return state = {
                ...state,
                selectLevel: null
            }
        case ENTER_ROOM:
            return state = {
                ...state,
                room: action.payloadRoom
            }
        case EXIT_ROOM:
            return state = {
                ...state,
                room: "000",
            }
        case BACK_TO_LEVEL_SELECT:
            return state = {
                ...state,
                selectLevel: null,
            }
        case BACK_TO_START:
            return state = {
                ...state,
                selectPlay: null,
            }
        case RESET_MAIN:
            return state = {
                currentBalance: 0,
                balanceChips: 0,
                balanceDiamond: 0,
                balanceOrium: 0,
                selectPlay: null,
                selectLevel: null,
                room: "000",
            }
        default:
            return state
    }
}

export const chipsSelect = () => {
    return dispatch => {
        dispatch({
            type: CHIPS_SELECT,
        })
    }
}

export const diamondSelect = () => {
    return dispatch => {
        dispatch({
            type: DIAMOND_SELECT,
        })
    }
}

export const oriumSelect = () => {
    return dispatch => {
        dispatch({
            type: ORIUM_SELECT,
        })
    }
}

export const playUnset = () => {
    return dispatch => {
        dispatch({
            type: PLAY_UNSET,
        })
    }
}

export const receiveBalance = (chips, diamond, orium) => {
    return dispatch => {
        dispatch({
            type: RECEIVE_BALANCE,
            payloadChips: chips,
            payloadDiamond: diamond,
            payloadOrium: orium,
        })
    }
}

export const selectLevel = (lvl) => {
    return dispatch => {
        dispatch({
            type: LEVEL_SELECT,
            payloadLvl: lvl
        })
    }
}

export const levelUnset = () => {
    return dispatch => {
        dispatch({
            type: LEVEL_UNSET,
        })
    }
}

export const exitRoom = () => {
    return dispatch => {
        dispatch({
            type: EXIT_ROOM
        })
    }
}

export const backToLevelSelect = () => {
    return dispatch => {
        dispatch({
            type: BACK_TO_LEVEL_SELECT
        })
    }
}

export const backToStart = () => {
    return dispatch => {
        dispatch({
            type: BACK_TO_START
        })
    }
}

export const enterRoom = (room) => {
    return dispatch => {
        dispatch({
            type: ENTER_ROOM,
            payloadRoom: room
        })
    }
}

export const resetMain = () => {
    return dispatch => {
        dispatch({
            type: RESET_MAIN
        })
    }
}

export default main;