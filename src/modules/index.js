import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import user from './user'
import main from './main';
import game from './game';
import seat from './seat';

export default combineReducers({
  routing: routerReducer,
  user: user,
  main: main,
  game: game,
  seat: seat,
})