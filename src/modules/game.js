const AFTER_FIN = 'AFTER_FIN';
const START = 'START';
const FINISH = 'FINISH';
const SET_RESULT = 'SET_RESULT';
const ADD_MONEY_BET = 'ADD_MONEY_BET';
const ADD_NAME_BET = 'ADD_NAME_BET';
const SET_BANKER = 'SET_BANKER';
const UNSET_BANKER = 'UNSET_BANKER';
const RESET_GAME = 'RESET_GAME';
const SET_TURN = 'SET_TURN';
const SET_CROSS = 'SET_CROSS';
const UNSET_CROSS = 'UNSET_CROSS';
const ADD_MIN_MAX = 'ADD_MIN_MAX';

const initialState = {
    turns: 0,
    isBanker: false,
    isStart: false,
    minBet: null,
    maxBet: null,
    banker: null,
    result: {1:"a", 2:"a", 3:"a"},
    isCross: false,
    betList: {
        "tiger": 0,
        "chicken": 0,
        "calabash": 0,
        "shrimp": 0,
        "fish": 0,
        "crab": 0,
    },
    selectedBet: "",
    logResult: []
}

const game = (state = initialState, action) => {
    switch (action.type) {
        case SET_TURN:
            return state = {
                ...state,
                turns: action.payloadTurn
            }
        case AFTER_FIN:
            return state = {
                ...state,
                selectedBet: "",
                isCross: false,
                betList: {
                    "tiger": 0,
                    "chicken": 0,
                    "calabash": 0,
                    "shrimp": 0,
                    "fish": 0,
                    "crab": 0,
                },
                logResult: state.result
            }
        case START:
            return state = {
                ...state,
                isStart: true,
                turns: state.turns + 1
            }
        case FINISH:
            return state = {
                ...state,
                isStart: false,
            }
        case ADD_MIN_MAX:
            return state = {
                ...state,
                minBet: action.payloadMinBet,
                maxBet: action.payloadMaxBet,
                banker: action.payloadBanker,
            }
        case SET_RESULT:
            return state = {
                ...state,
                result: action.payloadResult,
            }
        case ADD_MONEY_BET:
            return state = {
                ...state,
                betList: {
                    ...state.betList,
                    [action.payloadName]: state.betList[action.payloadName] + action.payloadBet,
                }
            }
        case ADD_NAME_BET:
            return state = {
                ...state,
                selectedBet: action.payloadNameBet
            }
        case SET_BANKER:
            return state = {
                ...state,
                isBanker: true,
            }
        case UNSET_BANKER:
            return state = {
                ...state,
                isBanker: false,
            }
        case SET_CROSS:
            return state = {
                ...state,
                isCross: true,
            }
        case UNSET_CROSS:
            return state = {
                ...state,
                isCross: false,
                selectedBet: ""
            }
        case RESET_GAME:
            return state = {
                turns: 0,
                isBanker: false,
                isStart: false,
                result: {1:"a", 2:"a", 3:"a"},
                minBet: null,
                maxBet: null,
                banker: null,
                isCross: false,
                betList: {
                    "tiger": 0,
                    "chicken": 0,
                    "calabash": 0,
                    "shrimp": 0,
                    "fish": 0,
                    "crab": 0,
                },
                selectedBet: "",
                logResult: [
                ]
            }
        default:
            return state
    }
}

export const setCross = () => {
    return dispatch => {
        dispatch({
            type: SET_CROSS,
        })
    }
}
export const unsetCross = () => {
    return dispatch => {
        dispatch({
            type: UNSET_CROSS,
        })
    }
}

export const setTurn = (turns) => {
    return dispatch => {
        dispatch({
            type: SET_TURN,
            payloadTurn: turns,
        })
    }
}

export const afterFin = () => {
    return dispatch => {
        dispatch({
            type: AFTER_FIN,
        })
    }
}

export const start = () => {
    return dispatch => {
        dispatch({
            type: START,
        })
    }
}

export const finish = () => {
    return dispatch => {
        dispatch({
            type: FINISH,
        })
    }
}

export const setResult = (result) => {
    return dispatch => {
        dispatch({
            type: SET_RESULT,
            payloadResult: result,
        })
    }
}

export const addMoneyBet = (name, bet) => {
    return dispatch => {
        dispatch({
            type: ADD_MONEY_BET,
            payloadName: name,
            payloadBet: bet,
        })
    }
}

export const addNameBet = (name) => {
    return dispatch => {
        dispatch({
            type: ADD_NAME_BET,
            payloadNameBet: name,
        })
    }
}

export const setBanker = () => {
    return dispatch => {
        dispatch({
            type: SET_BANKER
        })
    }
}

export const unsetBanker = () => {
    return dispatch => {
        dispatch({
            type: UNSET_BANKER
        })
    }
}

export const resetGame = () => {
    return dispatch => {
        dispatch({
            type: RESET_GAME
        })
    }
}

export const addMinMax = (min, max, banker) => {
    return dispatch => {
        dispatch({
            type: ADD_MIN_MAX,
            payloadMinBet: min,
            payloadMaxBet: max,
            payloadBanker: banker,
        })
    }
}

export default game;