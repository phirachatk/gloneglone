const LOG_IN = 'LOG_IN';
const LOG_OUT = 'LOG_OUT';

const initialState = {
    player_id: -1,
    picUrl: "",
    userName: "",
    token: null,
    isLogin: false,
}

const user = (state = initialState, action) => {
    switch (action.type) {
        case LOG_IN:
            return state = {
                token: action.payloadToken,
                picUrl: action.payloadUrl,
                userName: action.payloadName,
                player_id: action.payloadPlayerID,
                isLogin: true,
            } 
        case LOG_OUT:
            return state = {
                picUrl: "",
                name: "",
                player_id: -1,
                token: null,
                isLogin: false,
            }
        default:
            return state
    }
}

export const LoginToPage = (url, name, token, id) => {
    return dispatch  => {
        dispatch({
            type: LOG_IN,
            payloadUrl: url,
            payloadName: name,
            payloadToken: token,
            payloadPlayerID: id,
        })
    }
}
export const LogOutToPage = () => {
    return dispatch  => {
        dispatch({
            type: LOG_OUT,
        })
    }
}

export default user;