const SIT = 'SIT';
const GET_UP = 'GET_UP';
const SET_SEATS = 'SET_SEATS';
const UNSET_SEATS = 'UNSET_SEATS';
const RESET_SEAT = 'RESET_SEAT';

const initialState = {
    sitPos: "",
    isSit: false,
    seatList: {},
}

const seat = (state = initialState, action) => {
    switch (action.type) {
        case SIT:
            return state = {
                ...state,
                isSit: true,
                sitPos: action.payloadSitPos
            }
        case GET_UP:
            return state = {
                ...state,
                isSit: false,
                sitPos: "",
            }
        case SET_SEATS:
            return state = {
                ...state,
                seatList: action.payloadSeatList
            }
        case UNSET_SEATS:
            return state = {
                ...state,
                seatList: {},
            }
        case RESET_SEAT:
            return state = {
                sitPos: "",
                isSit: false,
                seatList: {},
            }
        default:
            return state
    }
}

export const sit = (pos) => {
    return dispatch => {
        dispatch({
            type: SIT,
            payloadSitPos: pos
        })
    }
}

export const getUp = () => {
    return dispatch => {
        dispatch({
            type: GET_UP,
        })
    }
}

export const setSeat = (seats) => {
    return dispatch => {
        dispatch({
            type: SET_SEATS,
            payloadSeatList: seats
        })
    }
}

export const unsetSeat = () => {
    return dispatch => {
        dispatch({
            type: UNSET_SEATS
        })
    }
}

export const resetSeat = () => {
    return dispatch => {
        dispatch({
            type: RESET_SEAT
        })
    }
}


export default seat;