import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import firebase from 'firebase'

import {
    enterRoom,
    levelUnset,
} from './../modules/main'

import {
    addMinMax,
} from './../modules/game'

import {
    sit
} from './../modules/seat'

import RoomTitle from '../components/roomSelect/RoomTitle';

class RoomSelect extends Component {

    constructor() {
        super();
        this.state = {
            roomList: null,
            isLoad: false,
            banker: null,
        }
    }

    clickToSelect = () => {
        this.props.levelUnset();
        this.props.changeLvlSelectPage();
    }

    playThisRoom = (no) => {
        this.props.enterRoom(no)
        if (this.state.roomList[no].seats === undefined) {
            this.props.sit(0)
        } else {
            for (let index = 0; index < 8; index++) {
                if (this.state.roomList[no].seats[index] === undefined) {
                    this.props.sit(index)
                    break;
                }
            }
        }
        this.props.changeToPlay();
    }

    componentWillMount = () => {
        let dbRef = firebase.database().ref().child('room/' + this.props.selectPlay + "/" + this.props.selectLevel);
        dbRef.on('value', snapshot => {
            this.setState({ roomList: snapshot.val()['room'], isLoad: true, }, () => this.props.addMinMax(snapshot.val()['min'], snapshot.val()['max'], snapshot.val()['banker']))
        })
    }

    render() {
        return (
            <div>
                <h3 className="text-center text-uppercase"><i className="fa fa-arrow-left mt-2 mr-2" onClick={this.clickToSelect.bind(this, 'Gold')}></i>{this.props.selectLevel} server</h3>
                {!this.state.isLoad ? null : <RoomTitle list={this.state.roomList} min={this.props.min} max={this.props.max} banker={this.props.banker} currency={this.props.selectPlay} onClick={this.playThisRoom} />}
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    isLogin: state.user.isLogin,
    selectPlay: state.main.selectPlay,
    selectLevel: state.main.selectLevel,
    min: state.game.minBet,
    max: state.game.maxBet,
    banker: state.game.banker
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    levelUnset,
    enterRoom,
    addMinMax,
    sit,
    changeToLogin: () => push('/'),
    changeLvlSelectPage: () => push('/main/levelselect'),
    changeToPlay: () => push('/game'),
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(RoomSelect)
