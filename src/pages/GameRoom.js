import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import firebase from 'firebase'
import axios from 'axios'

import {
    exitRoom,
    receiveBalance,
} from './../modules/main'

import {
    getUp,
} from './../modules/seat'

import {
    resetGame,
    unsetCross,
} from './../modules/game'

import { ip } from '../ip'
import Seats from '../components/game/Seats';
import LogResult from './../components/game/LogResult'
import ControllerPanel from '../components/game/ControllerPanel';
import Table from './../components/game/Table';
// import NavbarRoom from './../components/Layout/navbarLocate';
import Navbar from './../components/Layout/navbar';

class GameRoom extends Component {

    componentWillMount = () => {
        // if (this.props.room === "000") {
        //     this.props.changeRoomSelect();
        // }
        if(this.props.sitPos === ""){
            this.props.changeRoomSelect()
        }
    }

    componentDidMount = () => {
        window.onunload = (ev) => {
            if (this.props.isSit) {
                this.props.getUp();
                this.props.unsetCross();
                let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/seats/" + this.props.sitPos);
                dbRef.remove()
            }
        }
    }

    componentDidUpdate = () => {
        const url = ip + '/api/v1/user'
        axios.get(url, {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '.concat(localStorage.getItem('glone_token'))
            }
        }).then(response => {
            this.props.receiveBalance(response.data.data.chips_balance, response.data.data.diamonds_balance, response.data.data.coins_balance)
        }).catch(e => {
            console.log(e)
        })
    }


    componentWillUnmount = () => {
        if (this.props.isSit) {
            this.props.getUp();
            this.props.unsetCross();
            let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/seats/" + this.props.sitPos);
            dbRef.remove()
        }
        if (this.props.isBanker) {
            let dbIsStart = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
            dbIsStart.update({ isStart: false, times: -3 })
        }
        this.props.resetGame();
    }

    clickToRoomSelect = () => {
        this.props.changeRoomSelect()
    }
    render() {
        return (
            <div className="">
                <Navbar name={this.props.name} playerId={this.props.player_id} chips={this.props.chips} diamond={this.props.diamond} orium={this.props.orium} url={this.props.url} logOut={this.logOutButton} />
                <div className="startoni">
                    <div className="row ">
                        <div className="col-md-12 text-right">
                            <h3 className="my-3 text-uppercase text-bold">
                                {this.props.selectLevel + ' ' + this.props.room}  
                                <i className="btn btn-xs red" onClick={this.clickToRoomSelect}>X</i>
                            </h3>
                            {/* <NavbarRoom play={this.props.selectPlay} name={this.props.name} balance={this.props.balance} level={this.props.selectLevel} room={this.props.room} /> */}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <Seats />
                            <Table />
                            <ControllerPanel />
                            <LogResult />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    resetGame,
    getUp,
    exitRoom,
    unsetCross,
    receiveBalance,
    changeRoomSelect: () => push('/main/roomselect'),
    changeStart: () => push('/main/start')
}, dispatch)

const mapStateToProps = (state) => ({
    //
    // ─── USER STATE ───────────────────────────────────────────────────────────────────────────
    //
    name: state.user.userName,
    player_id: state.user.player_id,
    url: state.user.picUrl,
    //
    // ─── MAIN STATE ─────────────────────────────────────────────────────────────────
    //
    room: state.main.room,
    selectLevel: state.main.selectLevel,
    selectPlay: state.main.selectPlay,
    balance: state.main.currentBalance,
    chips: state.main.balanceChips,
    diamond: state.main.balanceDiamond,
    orium: state.main.balanceOrium,
    // ────────────────────────────────────────────────────────────────────────────────
    //
    // ─── SEAT STATE ─────────────────────────────────────────────────────────────────
    //
    sitPos: state.seat.sitPos,
    isSit: state.seat.isSit,
    // ────────────────────────────────────────────────────────────────────────────────
    // ────────────────────────────────────────────────────────────────────────────────
    isBanker: state.game.isBanker

})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(GameRoom)