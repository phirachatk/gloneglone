import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'

import {
    chipsSelect,
    diamondSelect,
    oriumSelect,
} from './../modules/main'

// import Result from './../components/game/Result'

const CardGroup = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
`;

const Card = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-content: center;
    align-items: center;
    background-color: rgba(255, 255, 255, 0.3);
    border: 3px solid #262626;
    border-radius: 10px;
    margin: 10px;
    padding: 10px;
`;
const CardImage = styled.img`
    width: 100px;
    height: auto;
`;
const CardName = styled.p`
    display: block;
    margin-bottom: 0;
    font-size: 1.1rem;
`;

class Start extends Component {
    clickToSelect = (level) => {
        if (this.props.isLogin) {
            let isStart = true;
            if (level === 'chips') {
                this.props.chipsSelect();
            } else if (level === 'diamond') {
                this.props.diamondSelect();
            } else if (level === 'orium') {
                this.props.oriumSelect();
            } else {
                isStart = false;
            }
            if (isStart) {
                this.props.changeLvlSelectPage()
            }
        }
    }
    render() {
        return (
            <div>
                <div className="text-center">
                    <h1 className="">Glone Glone</h1>
                </div>
                <CardGroup>
                    <Card >
                        <CardName className="text-uppercase">CHIPS</CardName>
                        <CardImage src={require('./../assets/images/game/table/money/money-min.png')} alt="gold sever" width="60%" />
                        <div className="btn-sm green mt-3" onClick={this.clickToSelect.bind(this, 'chips')}>Choose</div>
                    </Card>
                    <Card >
                        <CardName className="text-uppercase">DIAMOND</CardName>
                        <CardImage src={require('./../assets/images/main/gem.png')} alt="diamond sever" width="60%"  />
                        <div className="btn-sm green mt-3" onClick={this.clickToSelect.bind(this, 'diamond')}>Choose</div>
                    </Card>
                    <Card >
                        <CardName className="text-uppercase">ORIUM</CardName>
                        <CardImage className="rounded-circle" src={require('./../assets/images/login/orium-logo.gif')} alt="gold sever" width="60%"  />
                        <div className="btn-sm green mt-3" onClick={this.clickToSelect.bind(this, 'orium')}>Choose</div>
                    </Card>
                </CardGroup>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isLogin: state.user.isLogin,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    diamondSelect,
    chipsSelect,
    oriumSelect,
    changeLoginPage: () => push('/'),
    changeLvlSelectPage: () => push('/main/levelselect')
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Start)