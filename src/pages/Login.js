import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { LoginToPage } from '../modules/user';
import axios from 'axios'
import firebase from 'firebase'
import { ip } from '../ip'

import {
    receiveBalance,
    resetMain
} from './../modules/main'

import { resetGame } from './../modules/game'

import { LogOutToPage } from './../modules/user'

import Agree from '../components/Layout/agreeTnP'

class Login extends Component {

    constructor() {
        super();
        this.state = {
            isChecked: false,
        }
    }

    componentWillMount = () => {
        this.props.LogOutToPage()
        this.props.resetMain()
        this.props.resetGame()
    }

    postToSignUp = (responseFb) => {
        const url = ip + '/api/v1/user'
        axios.post(url, {
            "name": responseFb.name,
            "email": responseFb.id + "@facebook.com",
            "facebook_id": responseFb.id,
            "avatar": responseFb.picture.data.url,
            "password": responseFb.id,
        },
            {
                header: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then((response) => {
                firebase.auth().createUserWithEmailAndPassword(responseFb.id + "@facebook.com", responseFb.id)
                this.postToGetToken(responseFb)
            }).catch(e => {
                alert('error: sign in failed please try again later')
                console.log(e)
            })
    }

    getToSignIn = (token, profilePic) => {
        const url = ip + '/api/v1/user'
        axios.get(url, {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '.concat(token)
            }
        }).then(response => {
            this.props.LoginToPage(profilePic, response.data.data.display_name, token, response.data.data.player_id)
            this.props.changePage()
        }).catch(e => {
            alert('error: sign in failed please try again later')
            console.log(e)
        })
    }

    postToGetToken = (responseFb) => {
        const url = ip + '/oauth/token'
        axios.post(url, {
            grant_type: 'password',
            client_id: '2',
            client_secret: 'pMNTQAmNg05zzyF38QL857C66wjFlH0KBuKI6dLC',
            username: (responseFb.id).concat("@facebook.com"),
            password: responseFb.id
        }).then((response) => {
            localStorage.setItem('glone_token', response.data.access_token)
            firebase.auth().signInWithEmailAndPassword("1770614106326454@facebook.com", "1770614106326454")
            this.getToSignIn(response.data.access_token, responseFb.picture.data.url)
        }).catch(e => {
            localStorage.removeItem('glone_token')
            this.postToSignUp(responseFb);
        })
    }

    onCheck = () => {
        this.setState({ isChecked: !this.state.isChecked })
    }

    responseFacebook = (responseFb) => {
        if (localStorage.getItem('glone_token') && true) {
            this.getToSignIn(localStorage.getItem('glone_token'), responseFb.picture.data.url)
        } else {
            this.postToGetToken(responseFb)
        }
    }
    render() {
        return (
            <div className="text-center mt-4">
                <img className="rounded-circle" src={require('./../assets/images/login/orium-logo.gif')} alt="orium logo" height="120" />
                <h2 className=" text-light text-bold"> GAMES </h2>

                <br />
                <FacebookLogin
                    appId="606568263055224"
                    autoLoad={false}
                    fields="name,email,picture"
                    callback={this.state.isChecked ? this.responseFacebook : () => alert('please read and agree our term and policy first')} />
                <br />
                <Agree onCheck={this.onCheck} />
            </div>
        )
    };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    receiveBalance,
    LoginToPage,
    LogOutToPage,
    resetMain,
    resetGame,
    changePage: () => push('/main/games')
}, dispatch)

export default connect(
    null,
    mapDispatchToProps,
)(Login)