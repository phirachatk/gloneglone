import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class Redirect extends Component {

  componentDidMount() {
    this.props.changePage();
  }

  render() {
    return (<div></div>)
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  changePage: () => push('/main/start')
}, dispatch)

export default connect(
  null,
  mapDispatchToProps
)(Redirect)