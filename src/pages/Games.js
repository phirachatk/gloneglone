import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Result from './../components/game/Result'

// const CardGroup = styled.div`
//     display: flex;
//     flex-direction: row;
//     width: 100%;
// `;

// const Card = styled.div`
//     flex: 1;
//     display: flex;
//     flex-direction: column;
//     align-content: center;
//     align-items: center;
//     background-color: rgba(255, 255, 255, 0.3);
//     border: 3px solid #262626;
//     border-radius: 10px;
//     margin: 10px;
//     padding: 10px;
// `;
// const CardImage = styled.img`
//     width: 100px;
//     height: auto;
// `;
// const CardName = styled.p`
//     display: block;
//     margin-bottom: 0;
//     font-size: 1.1rem;
// `;

class Games extends Component {
    clickToSelect = (level) => {
        switch (level) {
            case 'gloneglone':
                this.props.changePage()
                break;

            default:
                break;
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-3 text-center px-2">
                        <div className="blue my-3 py-5 px-3" onClick={this.clickToSelect.bind(this, 'gloneglone')}>
                            <Result show="default" width="40px" />
                            <h1 className=" text-uppercase"><small>Glone</small><br />Glone</h1>
                        </div>
                    </div>

                    <div className="col-xs-3 text-center px-2">
                        <div className="green my-3 py-5 px-3" onClick={null}>
                            <Result show="default" width="40px" />
                            <h1 className=" text-uppercase"><small>Bing</small><br />Go!</h1>
                        </div>
                    </div>

                    <div className="col-xs-3 text-center px-2">
                        <div className="red my-3 py-5 px-3" onClick={null}>
                            <Result show="default" width="40px" />
                            <h1 className=" text-uppercase"><small>Cryp</small><br />Dice</h1>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    changePage: () => push('/main/start'),
    changeLoginPage: () => push('/')
}, dispatch)

export default connect(
    null,
    mapDispatchToProps,
)(Games)