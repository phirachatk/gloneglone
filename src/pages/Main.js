import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import axios from 'axios'

import Start from './Start'
import Games from './Games'
import LevelSelect from './LevelSelect';
import RoomSelect from './RoomSelect';

import {
  LogOutToPage,
} from './../modules/user'
import {
  receiveBalance,
  diamondSelect,
  chipsSelect,
  oriumSelect,
} from './../modules/main'

import Navbar from '../components/Layout/navbar';
import Footer from '../components/Layout/footer';
import { ip } from '../ip'

class Main extends Component {

  componentWillMount = () => {
    if (!this.props.isLogin) {
      this.props.changeLoginPage()
    } else {
      const url = ip + '/api/v1/user'
      axios.get(url, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '.concat(localStorage.getItem('glone_token'))
        }
      }).then(response => {
        console.log(response)
        this.props.receiveBalance(response.data.data.chips_balance, response.data.data.diamonds_balance, response.data.data.coins_balance)
      }).catch(e => {
        console.log(e)
      })
      if (this.props.selectPlay !== null || this.props.selectLevel !== null) {
        this.changeCurrency()
        if (this.props.selectLevel === null && this.props.selectPlay !== null) {
          this.props.changeLevelSelect();
        } else {
          this.props.changeRoomSelect();
        }
      } else {
        this.props.changeStartPage()
      }
    }
  }

  changeCurrency = () => {
    if (this.props.selectPlay === 'chips') {
      this.props.chipsSelect();
    } else if (this.props.selectPlay === 'diamond') {
      this.props.diamondSelect();
    } else if (this.props.selectPlay === 'orium') {
      this.props.oriumSelect();
    } else {
      this.props.changeStartPage();
    }
  }

  logOutButton = () => {
    localStorage.removeItem('glone_token')
    this.props.LogOutToPage();
    this.props.changeLoginPage();
  }

  render() {
    return (
        <div className="startoni">
          <Navbar name={this.props.name} playerId={this.props.playerId} chips={this.props.chips} diamond={this.props.diamond} orium={this.props.orium} url={this.props.url} logOut={this.logOutButton} />
          <Route path="/main/games" component={Games} />
          <Route path="/main/start" component={Start} />
          <Route path="/main/levelselect" component={LevelSelect} />
          <Route path="/main/roomselect" component={RoomSelect} />
          <Footer />
        </div>

    )
  }
}

const mapStateToProps = (state) => ({
  url: state.user.picUrl,
  name: state.user.userName,
  playerId: state.user.player_id,
  isLogin: state.user.isLogin,
  chips: state.main.balanceChips,
  diamond: state.main.balanceDiamond,
  orium: state.main.balanceOrium,
  balance: state.main.currentBalance,
  selectPlay: state.main.selectPlay,
  selectLevel: state.main.selectLevel,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  LogOutToPage,
  receiveBalance,
  diamondSelect,
  chipsSelect,
  oriumSelect,
  changeLevelSelect: () => push('/main/levelselect'),
  changeRoomSelect: () => push('/main/roomselect'),
  changeStartPage: () => push('/main/games'),
  changeLoginPage: () => push('/'),
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main)

