import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import styled from 'styled-components';

import {
    selectLevel,
    playUnset,
} from './../modules/main'
import Level from '../components/levelSelect/Level';
import silver from '../assets/images/levelSelect/lv_silver.png';
import gold from '../assets/images/levelSelect/lv_gold.png';
import ruby from '../assets/images/levelSelect/lv_ruby.png';
import diamond from '../assets/images/levelSelect/lv_diamond.png';
import platinum from '../assets/images/levelSelect/lv_platinum.png';

const CardGroup = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
`;


class LevelSelect extends Component {
    componentWillMount = () => {
        if (!this.props.isLogin) {
            this.props.changeToLogin();
        }
    }

    clickToMain = () => {
        this.props.playUnset()
        this.props.changeStartPage()
    }

    clickToRoomSelect = (name) => {
        this.props.selectLevel(name);
        this.props.changeRoomSelect()
    }

    render() {
        const levels = [
            {
                name: 'silver',
                image: silver,
            },
            {
                name: 'gold',
                image: gold,
            },
            {
                name: 'ruby',
                image: ruby,
            },
            {
                name: 'diamond',
                image: diamond,
            },
            {
                name: 'platinum',
                image: platinum,
            },
        ];
        return (
            <div>
                <h3 className="text-center text-uppercase"><i className="fa fa-arrow-left mt-2 mr-2"  onClick={this.clickToMain}></i>{this.props.selectPlay} Play</h3>
                <CardGroup>
                    {levels.map((level, index) => {
                        return <Level
                            key={index}
                            name={level.name}
                            image={level.image}
                            onClick={this.clickToRoomSelect.bind(this, level.name)}
                        />
                    })}
                </CardGroup>
                
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    isLogin: state.user.isLogin,
    selectPlay: state.main.selectPlay,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    playUnset,
    selectLevel,
    changeToLogin: () => push('/'),
    changeStartPage: () => push('/main/start'),
    changeRoomSelect: () => push('/main/roomselect')
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(LevelSelect)