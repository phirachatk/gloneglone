import React, { Component } from 'react'

export default class RoomTitle extends Component {
    render() {
        return (
            <div>
                <table className="table table-dark">
                    <thead>
                        <tr>
                            <td>Room</td>
                            <td>Players</td>
                            <td>Bet size</td>
                            <td>Banker cost</td>
                            <td className="text-right"> </td>
                        </tr>
                    </thead>
                    <tbody>
                        {Object.keys(this.props.list).map((item, index) => 
                        <tr key={index} onClick={this.props.onClick.bind(this, item)}>
                            <td>{item}</td>
                            <td>{this.props.list[item]["seats"] == null ? 0 : Object.keys(this.props.list[item]["seats"]).length} / 8</td>
                            <td><img src={require('./../../assets/images/currency/diamond.png')} alt={this.props.currency} height="24"/> {this.props.min} - {this.props.max}</td>
                            <td>{this.props.banker}</td>
                            <td className="text-center"> <div className="btn-sm green">PLAY</div> </td>
                        </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}
