import React, { Component } from 'react';
import styled from 'styled-components';

const Card = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-content: center;
    align-items: center;
    background-color: rgba(255, 255, 255, 0.3);
    border: 3px solid #262626;
    border-radius: 10px;
    margin: 10px;
    padding: 10px;
`;
const CardImage = styled.img`
    width: 100px;
    height: auto;
`;
const CardName = styled.p`
    display: block;
    margin-bottom: 0;
    font-size: 1.1rem;
`;

// const CardButton = styled.button`
//         font-size: 0.5rem;
//         padding-top: 5px;
//         line-height: 1;
//         min-width: 100px;
//         cursor: pointer;   
//         margin: 15px 0; 
// `;

export default class Level extends Component {
    render() {
        const { name, image, onClick} = this.props;
        return (
            <Card >
                <CardName className="text-uppercase">{name}</CardName>
                <CardImage src={image} alt="level" />
                {/* <p>choose room</p> */}
                <div className="btn-sm green"  onClick={onClick}>PLAY</div>
            </Card>
            
        )
    }
}
