import React, { Component } from 'react'

export default class Timer extends Component {
    render() {
        return (
            <div>
                {this.props.isStart ? (this.props.elapsed < 0 ? 'shake a dice' : 'time: ' + (22 - this.props.elapsed)) : ((this.props.isBanker && this.props.isSit) ? <div className="btn-xs blue mb-3" onClick={this.props.onClick}>start</div> : 'waiting for banker')}
            </div>
        )
    }
}
