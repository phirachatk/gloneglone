import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import firebase from 'firebase'

import {
  sit,
  setSeat,
} from './../../modules/seat'

import {
  setBanker,
  unsetBanker
} from './../../modules/game'

import Seat from './Seat';
import Seatable from './Seatable';

class Seats extends Component {

  constructor() {
    super();
    this.checkBanker = this.checkBanker.bind(this);
  }

  componentWillMount = () => {
    this.checkBanker(this.props.balance) ? this.props.setBanker() : this.props.unsetBanker()
    let dbRef2 = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/seats/" + this.props.sitPos);
    dbRef2.update({ name: this.props.name, avatar_url: this.props.picUrl, balance: this.props.balance, isSit: true, isBanker: this.checkBanker(this.props.balance), player_id: this.props.player_id })
 
    let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/seats/");
    dbRef.on('value', snapshot => {
      this.props.setSeat(snapshot.val())
    })
  }

  checkBanker = (balance) => {
    let temp = true;
    if (this.props.seatList === null) {
      temp = balance*1 > this.props.banker
    } else {
      if (temp) {
        for (const key in this.props.seatList) {
          if (this.props.seatList[key].isBanker) {
            temp = false;
          }
        }
      }
      if (temp && balance*1 < this.props.banker*1) {
        temp = false;
      }
    }
    return temp;
  }

  // selectSeat = (no) => {
  //   if (this.props.balance > 0) {
  //     this.checkBanker(this.props.balance) ? this.props.setBanker() : this.props.unsetBanker()
  //     let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/seats/" + no);
  //     dbRef.update({ name: this.props.name, avatar_url: this.props.picUrl, balance: this.props.balance, isSit: true, isBanker: this.checkBanker(this.props.balance), player_id: this.props.player_id })
  //     this.props.sit(no);
  //   }
  // }
  render() {
    let seat = [0, 1, 2, 3, 4, 5, 6, 7]
    return (
      <div className="d-flex justify-content-between align-items-center mr-5 seatRow">
        {seat.map((item, index) => (this.props.seatList === null || this.props.seatList[item] === undefined) ?
          <Seatable key={index} canSit={this.props.isSit} /> :
          <Seat key={index}
            name={this.props.seatList[item].name}
            balance={this.props.seatList[item].balance}
            picUrl={this.props.seatList[item].avatar_url}
            isBanker={this.props.seatList[item].isBanker} />
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  name: state.user.userName,
  player_id: state.user.player_id,
  picUrl: state.user.picUrl,

  selectPlay: state.main.selectPlay,
  selectLevel: state.main.selectLevel,
  balance: state.main.currentBalance,
  room: state.main.room,

  isSit: state.seat.isSit,
  seatList: state.seat.seatList,
  sitPos: state.seat.sitPos,

  isBanker: state.game.isBanker,
  banker: state.game.banker
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  sit,
  setSeat,
  setBanker,
  unsetBanker,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Seats)