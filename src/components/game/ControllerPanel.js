import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import firebase from 'firebase'

import {
    addMoneyBet,
    setCross,
    unsetCross,
    addNameBet,
} from './../../modules/game'
import {
    getUp,
} from './../../modules/seat'

import Chip from './Chip';

import min from '../../assets/images/game/table/money/money-min.png'
import max from '../../assets/images/game/table/money/money-mid.png'
import mid from '../../assets/images/game/table/money/money-max.png'


class ControllerPanel extends Component {

    addBet = (val) => {
        if (!this.props.isBanker && this.props.isStart && (this.props.balance * 1 - this.calBet())) {
            switch (this.props.selectedBet) {
                case "tiger/chicken":
                case "chicken/tiger":
                    this.props.addMoneyBet("tiger", val / 2)
                    this.props.addMoneyBet("chicken", val / 2)
                    break;
                case "chicken/calabash":
                case "calabash/chicken":
                    this.props.addMoneyBet("chicken", val / 2)
                    this.props.addMoneyBet("calabash", val / 2)
                    break;
                case "tiger/crab":
                case "crab/tiger":
                    this.props.addMoneyBet("tiger", val / 2)
                    this.props.addMoneyBet("crab", val / 2)
                    break;
                case "chicken/fish":
                case "fish/chicken":
                    this.props.addMoneyBet("chicken", val / 2)
                    this.props.addMoneyBet("fish", val / 2)
                    break;
                case "calabash/crab":
                case "crab/calabash":
                    this.props.addMoneyBet("calabash", val / 2)
                    this.props.addMoneyBet("crab", val / 2)
                    break;
                case "shrimp/fish":
                case "fish/shrimp":
                    this.props.addMoneyBet("shrimp", val / 2)
                    this.props.addMoneyBet("fish", val / 2)
                    break;
                case "calabash/shrimp":
                case "shrimp/calabash":
                    this.props.addMoneyBet("calabash", val / 2)
                    this.props.addMoneyBet("shrimp", val / 2)
                    break;
                case "fish":
                case "crab":
                case "shrimp":
                case "calabash":
                case "tiger":
                case "chicken":
                    this.props.addMoneyBet(this.props.selectedBet, val);
                    break;
                default:
                    break;
            }
        }
    }

    getUp = () => {
        if (this.props.isSit) {
            let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/seats/" + this.props.sitPos);
            dbRef.remove()
            this.props.getUp()
            this.props.unsetCross()
        }
    }

    select2bet = () => {
        this.props.isCross ? this.props.unsetCross() : this.props.setCross()
    }

    calBet = () => {
        let sum = 0
        if (!this.props.isBanker) {
            for (const key in this.props.betList) {
                sum+=this.props.betList[key]
            }
        }
        return sum
    }

    render() {
        const value = [20, 50, 100, 250, 500, 1000, 2000]
        const chips = [
            {
                val: this.props.min,
                image: min,
            },
            {
                val: value.filter(item => item > this.props.min && item < this.props.max)[0],
                image: mid,
            },
            {
                val: this.props.max,
                image: max,
            },
        ];
        return (
            <div>
                {this.props.isSit ?
                    <div className="text-right mr-5">
                        {/* <div className="btn btn-xs blue mx-3" onClick={this.select2bet}>2 bet</div>
                        {chips.map((chip, index) =>
                            <Chip
                                key={index}
                                name={chip.val}
                                image={chip.image}
                                onClick={this.addBet.bind(this, chip.val)} />
                        )} */}
                        {!this.props.isBanker || !this.props.isStart ? <div className="btn btn-xs red mx-3" disabled={!this.props.isSit || this.props.isStart} onClick={this.getUp}>leave</div> : null}
                    </div> : null}
                <hr />
                {this.props.isSit ? 'you balance ' + (this.props.balance * 1 - this.calBet()) : null}
                {
                    this.props.isStart ?
                        <div>
                            {Object.keys(this.props.betList).map((item, index) => <li key={index}>{item}  {this.props.betList[item]}</li>)}
                        </div>
                        : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isSit: state.seat.isSit,
    isStart: state.game.isStart,
    betList: state.game.betList,
    selectedBet: state.game.selectedBet,
    balance: state.main.currentBalance,
    sitPos: state.seat.sitPos,
    room: state.main.room,
    selectPlay: state.main.selectPlay,
    selectLevel: state.main.selectLevel,
    isCross: state.game.isCross,
    min: state.game.minBet,
    max: state.game.maxBet,
    isBanker: state.game.isBanker,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    addMoneyBet,
    getUp,
    setCross,
    unsetCross,
    addNameBet,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ControllerPanel)