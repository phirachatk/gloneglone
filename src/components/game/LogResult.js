import React, { Component } from 'react'
import Result from './Result'
import { connect } from 'react-redux'

class LogResult extends Component {
  render() {
    return (
      <div>
        <h3>History</h3>
            {(this.props.logResult).map((item, index) =>
              <div key={index}>
                <Result show={this.props.turn !== 0 ? item : "default"} width="70px" />
              </div>
            )}
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  isLogin: state.user.isLogin,
  type: state.main.selectPlay,
  logResult: state.game.logResult,
  turn : state.game.turn,
})

export default connect(mapStateToProps)(LogResult)