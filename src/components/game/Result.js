import React, { Component } from 'react'

export default class Result extends Component {
  render() {
    let checker = {
      "a": "tiger",
      "b": "chicken",
      "c": "fish",
      "d": "crab",
      "e": "calabash",
      "f": "shrimp",
    }
    if (this.props.show === "default") {
      return (
        <div className="mb-3">
          <img src={require('./../../assets/images/login/dice1.png')} alt="dic1" width={this.props.width} />
          <img src={require('./../../assets/images/login/dice2.png')} alt="dic2" width={this.props.width} />
          <img src={require('./../../assets/images/login/dice3.png')} alt="dic3" width={this.props.width} />
        </div>
      )
    } else {
      return (
        <div className="mb-3">
          <img src={require('./../../assets/images/game/betItem/' + checker[this.props.show[1]] + '.png')} alt="dic1" height={this.props.width} />
          <img src={require('./../../assets/images/game/betItem/' + checker[this.props.show[2]] + '.png')} alt="dic2" height={this.props.width} />
          <img src={require('./../../assets/images/game/betItem/' + checker[this.props.show[3]] + '.png')} alt="dic3" height={this.props.width} />
        </div>
      )
    }
  }
}
