import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Process from '../../components/game/Process';

import {
    addNameBet,
} from './../../modules/game'

import ItemBet from './ItemBet';

import tiger from '../../assets/images/game/betItem/tiger.jpg';
import chicken from '../../assets/images/game/betItem/chicken.jpg';
import fish from '../../assets/images/game/betItem/fish.jpg';
import crab from '../../assets/images/game/betItem/crab.jpg';
import calabash from '../../assets/images/game/betItem/calabash.jpg';
import shrimp from '../../assets/images/game/betItem/shrimp.jpg';
import BetPanel from './BetPanel';


class Table extends Component {
    constructor() {
        super();
        this.state = {
            activeModal: false
        }
    }

    // //double bet algorithm TT
    // selectBet = (name) => {
    //     if (this.props.isStart) {
    //         if (this.props.isCross) {
    //             if (this.props.nameBet === name + '/') {
    //                 this.props.addNameBet('')
    //             } else {
    //                 if (this.props.nameBet === '') {
    //                     this.props.addNameBet(name + '/')
    //                 } else if (this.props.nameBet.split('/')[0] !== '' && this.checkCross(this.props.nameBet.split('/')[0], name)) {
    //                     if (this.props.nameBet.split('/')[1] === name) {
    //                         this.props.addNameBet(this.props.nameBet.split(name)[0])
    //                     } else if (this.props.nameBet.split('/')[1] !== name) {
    //                         this.props.addNameBet(this.props.nameBet.split('/')[0] + '/' + name)
    //                     }
    //                     if (this.props.nameBet[this.length - 1] === '/')
    //                         this.props.addNameBet(this.props.nameBet + name)
    //                 } else {
    //                     this.props.addNameBet(name + '/')
    //                 }
    //             }
    //         } else {
    //             this.props.addNameBet((name === this.props.nameBet) ? "" : name);
    //         }
    //     }
    // }

    // checkCross = (front, back) => {
    //     switch (front) {
    //         case 'tiger':
    //             if (back === 'chicken' || back === 'crab')
    //                 return true;
    //             break;
    //         case 'chicken':
    //             if (back === 'tiger' || back === 'fish' || back === 'calabash')
    //                 return true;
    //             break;
    //         case 'fish':
    //             if (back === 'chicken' || back === 'shrimp')
    //                 return true;
    //             break;
    //         case 'crab':
    //             if (back === 'tiger' || back === 'calabash')
    //                 return true;
    //             break;
    //         case 'calabash':
    //             if (back === 'crab' || back === 'chicken' || back === 'shrimp')
    //                 return true;
    //             break;
    //         case 'shrimp':
    //             if (back === 'fish' || back === 'calabash')
    //                 return true;
    //             break;
    //         default:
    //             return false;
    //     }
    //     return false;
    // }

    // canBet = (name) => {
    //     if (this.props.nameBet.split('/')[0] + '/' + name === this.props.nameBet || name + '/' + this.props.nameBet.split('/')[1] === this.props.nameBet) {
    //         return true;
    //     } else if (this.props.nameBet === name + '/') {
    //         return true;
    //     }
    //     return false;
    // }

    // componentDidUpdate = () => {
    //     if (this.props.nameBet !== '' && this.props.nameBet[this.length - 1] !== '/' && this.props.nameBet.split('/').length < 2 && this.props.isCross) {
    //         this.props.addNameBet('')
    //     }
    // }

    toggleModal = () => {
        this.setState({ activeModal: !this.state.activeModal })
    }

    selectBet = (name) => {
        if (this.props.isStart) {
            this.props.addNameBet(name);
            this.toggleModal()
        }
    }

    render() {
        const betItems = [
            {
                name: 'tiger',
                image: tiger,
            },
            {
                name: 'chicken',
                image: chicken,
            },
            {
                name: 'fish',
                image: fish,
            },
            {
                name: 'crab',
                image: crab,
            },
            {
                name: 'calabash',
                image: calabash,
            },
            {
                name: 'shrimp',
                image: shrimp,
            },
        ];
        return (
            <div className="">
                <div className="gameTable">
                    <div className="row">
                        <div className="col-md-2"></div>
                        <div className="col-md-8">
                            {betItems.map((item, index) =>
                                <ItemBet
                                    key={index}
                                    name={item.name}
                                    image={item.image}
                                    isStart={this.props.isStart}
                                    // canBet={((this.props.isCross && this.canBet(item.name)) || (!this.props.isCross && this.props.nameBet === item.name))}
                                    onClick={this.selectBet.bind(this, item.name)} />)
                            }
                        </div>
                        <div className="col-md-2"></div>
                    </div>
                </div>
                
                <div className={"modal fade" + (this.state.activeModal ? " show d-block" : " d-none")} onClick={this.toggleModal}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <BetPanel toggle={this.toggleModal}/>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-3">
                        <Process />  
                    </div>
                    <div className="col-md-9">
                        
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    nameBet: state.game.selectedBet,
    isStart: state.game.isStart,
    isCross: state.game.isCross
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    addNameBet,
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Table)