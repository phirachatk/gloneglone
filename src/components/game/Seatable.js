import React, { Component } from 'react'

export default class Seatable extends Component {
    render() {
        return (
            <div>
                <div className="btn btn-xs btn-secondary" onClick={this.props.canSit ? null : this.props.onSit}>Sit</div>
            </div>
        )
    }
}
