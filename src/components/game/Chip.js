import React, { Component } from 'react'

export default class Chip extends Component {
  render() {
    
    return (
      <a className="chips">
        <div className="chip-title">{this.props.name}</div>
        <img src={this.props.image} alt={this.props.name}
          onClick={this.props.onClick} />
      </a>
    )
  }
}
