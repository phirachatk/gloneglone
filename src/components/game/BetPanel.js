import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {
    addMoneyBet,
} from './../../modules/game'

import Chip from './Chip';

import min from '../../assets/images/game/table/money/money-min.png'
import max from '../../assets/images/game/table/money/money-mid.png'
import mid from '../../assets/images/game/table/money/money-max.png'


class BetPanel extends Component {
    

    calBet = () => {
        let sum = 0
        if (!this.props.isBanker) {
            for (const key in this.props.betList) {
                sum+=this.props.betList[key]
            }
        }
        return sum
    }

    addBet = (val) => {
        if (!this.props.isBanker && this.props.isStart && (this.props.balance * 1 - this.calBet())) {
            switch (this.props.selectedBet) {
                case "tiger/chicken":
                case "chicken/tiger":
                    this.props.addMoneyBet("tiger", val / 2)
                    this.props.addMoneyBet("chicken", val / 2)
                    break;
                case "chicken/calabash":
                case "calabash/chicken":
                    this.props.addMoneyBet("chicken", val / 2)
                    this.props.addMoneyBet("calabash", val / 2)
                    break;
                case "tiger/crab":
                case "crab/tiger":
                    this.props.addMoneyBet("tiger", val / 2)
                    this.props.addMoneyBet("crab", val / 2)
                    break;
                case "chicken/fish":
                case "fish/chicken":
                    this.props.addMoneyBet("chicken", val / 2)
                    this.props.addMoneyBet("fish", val / 2)
                    break;
                case "calabash/crab":
                case "crab/calabash":
                    this.props.addMoneyBet("calabash", val / 2)
                    this.props.addMoneyBet("crab", val / 2)
                    break;
                case "shrimp/fish":
                case "fish/shrimp":
                    this.props.addMoneyBet("shrimp", val / 2)
                    this.props.addMoneyBet("fish", val / 2)
                    break;
                case "calabash/shrimp":
                case "shrimp/calabash":
                    this.props.addMoneyBet("calabash", val / 2)
                    this.props.addMoneyBet("shrimp", val / 2)
                    break;
                case "fish":
                case "crab":
                case "shrimp":
                case "calabash":
                case "tiger":
                case "chicken":
                    this.props.addMoneyBet(this.props.selectedBet, val);
                    break;
                default:
                    break;
            }
        }
        this.props.toggle()
    }

    // select2bet = () => {
    //     this.props.isCross ? this.props.unsetCross() : this.props.setCross()
    // }

    render() {
        const value = [20, 50, 100, 250, 500, 1000, 2000]
        const chips = [
            {
                val: this.props.min,
                image: min,
            },
            {
                val: value.filter(item => item > this.props.min && item < this.props.max)[0],
                image: mid,
            },
            {
                val: this.props.max,
                image: max,
            },
        ];
        return (
            <div>
                <div className="text-right mr-5">
                    {/* <div className="btn btn-xs blue mx-3" onClick={this.select2bet}>2 bet</div> */}
                    {chips.map((chip, index) =>
                        <Chip
                            key={index}
                            name={chip.val}
                            image={chip.image}
                            onClick={this.addBet.bind(this, chip.val)} />
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    min: state.game.minBet,
    max: state.game.maxBet,
    isBanker: state.game.isBanker,
    isStart: state.game.isStart,
    balance: state.main.currentBalance
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    addMoneyBet
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(BetPanel)