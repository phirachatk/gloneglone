import React, { Component } from 'react'

export default class Seat extends Component {
    render() {
        return (
            <div>
                <img src={this.props.picUrl} alt={this.props.name} width="80px" />
                <p>{this.props.name}</p>
                <p className="mb-0">{this.props.balance}</p>
                {this.props.isBanker ? <p>Banker</p>:null}
            </div>
        )
    }
}