import React, { Component } from 'react';

// + (this.props.canBet && this.props.isStart ? "active" : "")

export default class ItemBet extends Component {
  render() {
    return (
      <a className={"bet "} onClick={this.props.onClick}>
        <img src={this.props.image}
          alt="" />
      </a>
    )
  }
}
