import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import firebase from 'firebase'
import axios from 'axios'
import Pusher from 'pusher-js'
import { ip } from '../../ip'

import {
    afterFin,
    setResult,
    finish,
    start,
    setTurn,
    setBanker,
    unsetBanker,
} from './../../modules/game'

import { getUp } from '../../modules/seat'

import Result from './Result';
import Timer from './Timer';

import '../../assets/css/cover-anim.css'

const TIME = 22;

class Process extends Component {
    constructor() {
        super();
        this.state = {
            elapsed: -3,
            now: new Date(),
            isShake: false,
            isReceiveBanker: false,
            isReceiveResult: false,
            isSendResutlt: false,
            countModal: 0,
            activeModal: false,
        }

    }

    componentWillMount = () => {
        if (!this.props.isBanker) {
            let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
            dbRef.on('value', snapshot => {
                if (snapshot.val().isStart) {
                    this.setState({ isSendResutlt: false, isReceiveBanker: false, isReceiveResult: false }, () => this.props.start())
                } else {
                    if (snapshot.val().times === TIME)
                        this.finRound()
                    this.props.finish()
                }
                this.props.setTurn(snapshot.val().turns)
            })
        }
    }

    componentDidMount = () => {
        const pusher = new Pusher('912267ab5e6a006f529c', {
            cluster: 'ap1',
            encrypted: true
        });
        const channel = pusher.subscribe("room-" + this.props.selectPlay + "-" + this.props.selectLevel + "-room-" + this.props.room);
        channel.bind('show-result', (result) => {
            if (!this.state.isReceiveResult) {
                this.props.setResult(result.result)
                this.setState({ isShake: false, isReceiveResult: true, activeModal: true, countModal: 0 });
            }
        });
        channel.bind('set-banker', (banker) => {
            if (!this.state.isReceiveBanker) {
                this.setState({ isReceiveBanker: true }, () => {
                    if (this.props.isBanker) {
                        let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/seats/" + this.props.sitPos);
                        dbRef.update({ isBanker: this.props.sitPos === banker.seatNumber })
                    }
                    banker.seatNumber === this.props.sitPos ? this.props.setBanker() : this.props.unsetBanker()
                })
            }
        });
        let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room + "/times");
        dbRef.on('value', snapshot => {
            this.setState({ elapsed: snapshot.val() })
        })
        window.onunload = () => {
            if (this.props.isBanker) {
                clearInterval(this.timerMain)
                this.props.finish()
                let dbIsStart = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
                dbIsStart.update({ isStart: false, times: -3 })
            }
        }
    }

    componentWillUpdate = () => {
        if (this.state.elapsed === 17 && !this.state.isShake) {
            this.setState({ isShake: true })
        }
    }

    componentDidUpdate = () => {
        if (this.state.activeModal && this.state.countModal === 0) {
            this.countModal();
        }
    }

    componentWillUnmount = () => {
        clearInterval(this.timerMain)
        if (this.props.isBanker) {
            let dbIsStart = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
            dbIsStart.update({ isStart: false, times: -3 })
        }
    }

    startRound = () => {
        if (this.props.isBanker && this.props.isSit && Object.keys(this.props.seatList).length > 1) {
            this.props.start();
            let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
            dbRef.update({ isStart: true })
            this.setState({ now: new Date(), elapsed: -3, isShake: false }, () => this.startTimer())
        }
    }
    startTimer = () => {
        this.timerMain = setInterval(() => {
            if (this.state.elapsed === TIME) {
                clearInterval(this.timerMain)
                this.finRound();
            }
            this.tick()
        }, 100);
    }

    tick = () => {
        let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
        dbRef.update({ times: -3 + (Math.floor((new Date() - this.state.now) / 1000)) })
    }

    countModal = () =>
        this.interval = setInterval(() => {
            if (this.state.countModal === 5) {
                clearInterval(this.interval)
                this.setState({ countModal: 0, activeModal: false }, () => {
                    if (this.props.isBanker) {
                        let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
                        dbRef.update({ isStart: true })
                    }
                })

            }
            this.setState({ countModal: this.state.countModal + 1 })
        }, 1000);

    // a = tiger b = chicken c = fish d = crab e = calabash f = shrimp
    finRound = () => {
        let dbRef = firebase.database().ref().child("room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room);
        this.setState({ elapsed: -3 }, () => dbRef.update({ isStart: false, times: -3, turns: this.props.turns + 1 }))
        if (!this.state.isSendResutlt) {
            const url = ip + '/api/v1/bet/send'
            axios.post(url,
                {
                    "player_id": this.props.player_id,
                    "status": (this.props.isBanker) ? "banker" : "player",
                    "currency": this.props.selectPlay,
                    "room_reference": "room/" + this.props.selectPlay + "/" + this.props.selectLevel + "/room/" + this.props.room,
                    "sit_position": this.props.sitPos,
                    "betLists": {
                        "a": this.props.betList["tiger"],
                        "b": this.props.betList["chicken"],
                        "c": this.props.betList["fish"],
                        "d": this.props.betList["crab"],
                        "e": this.props.betList["calabash"],
                        "f": this.props.betList["shrimp"],
                    }
                },
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '.concat(localStorage.getItem('glone_token')),
                    }
                }).then(() => {
                    this.setState({ isSendResutlt: true })
                }).catch(e => {
                    console.log(e)
                })
        }
    }

    render() {
        return (
            <div className="processGame text-center">
                <Timer isStart={this.props.isStart} isBanker={this.props.isBanker} elapsed={this.state.elapsed} isSit={this.props.isSit} onClick={this.startRound} />
                <div className={this.props.isStart ? "coverCover " : ""}>
                    <div className={this.props.isStart ? "coverShakeStart" : ""}>
                        <img className={(this.state.isShake && this.state.isStart ? "coverShake5s " : "")} src={require('./../../assets/images/game/table/cover.png')} alt="cover" height="175px" />
                    </div>
                </div>
                <Result show="default" width="50px" />

                <div className="col-md-2">
                    <div className={"modal fade" + (this.state.activeModal ? " show d-block" : " d-none")} >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <Result show={this.props.result} width="50px" />
                            </div>
                            <div className="modal-footer">
                                {this.state.countModal}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    player_id: state.user.player_id,

    elapsed: state.game.elapsed,
    isStart: state.game.isStart,
    result: state.game.result,
    betList: state.game.betList,
    turns: state.game.turns,
    isBanker: state.game.isBanker,

    selectPlay: state.main.selectPlay,
    selectLevel: state.main.selectLevel,
    room: state.main.room,

    isSit: state.seat.isSit,
    sitPos: state.seat.sitPos,
    seatList: state.seat.seatList,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    afterFin,
    setResult,
    finish,
    start,
    setTurn,
    setBanker,
    unsetBanker,
    getUp
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Process)