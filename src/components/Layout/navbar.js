import React, { Component } from 'react';

class Navbar extends Component {
    state = {  } 
    render() {
        return (

            // <nav className="navbar navbar-dark bg-dark">
            //     <span className="navbar-brand" href="#">
            //         <img src={this.props.url} width="60" height="60" class="d-inline-block align-top" alt=""/>
            //         {this.props.playerId}
            //     </span>
            //     <ul className="navbar-nav">
            //         <li className="nav-item">
            //             <div className="row">
            //                 <div className="col text-left">
            //                     <img src={require('./../../assets/images/game/table/money/money-min.png')} alt="chips" />
            //                 </div>
            //                 <div className="col text-right">
            //                     {this.props.goldBalance}
            //                 </div>
            //             </div>
            //         </li>
            //         <li className="nav-item">
            //             <div className="row">
            //                 <div className="col text-left">
            //                     <img src={require('./../../assets/images/game/table/money/money-min.png')} alt="chips" />
            //                 </div>
            //                 <div className="col text-right">
            //                     {this.props.goldBalance}
            //                 </div>
            //             </div>
            //         </li>
            //     </ul>
            // </nav>


            <nav>
                <ul className="nav nav-pills nav-fill">
                    <div className="profile">
                        <img src={this.props.url} alt="profilePic"  height="60px"/>
                    </div>
                    <li className="nav-item">
                        {this.props.playerId}
                    </li>
                    <li className="nav-item d-flex flex-row">
                        <img src={require('./../../assets/images/game/table/money/money-min.png')} alt="chips" />
                        {this.props.chips}
                        <i className="fa fa-plus-square text-success ml-1"></i>
                    </li>
                    <li className="nav-item d-flex flex-row">
                        <img src={require('./../../assets/images/currency/diamond.png')} alt="dimonds" />
                        {this.props.diamond}
                        <i className="fa fa-plus-square text-success ml-1"></i>
                    </li>
                    <li className="nav-item d-flex flex-row">
                        <img src={require('./../../assets/images/login/orium-logo.gif')} alt="coins" />
                        {this.props.orium} 
                        <i className="fa fa-plus-square text-success ml-1"></i>
                    </li>
                    <div className="nav-iconItem">
                        <img src={require('./../../assets/images/main/tools.png')} alt="tools" onClick={this.props.logOut} />  
                    </div>
                </ul>
            </nav>


        );
    }
}

export default Navbar;