import React, { Component } from 'react'
import gem from './../../assets/images/main/val_gem.png'
import coin from './../../assets/images/main/val_coin.png'

export default class NavLocation extends Component {
    render() {
        let uri
        uri = ((this.props.play === 'gold') ? coin : gem)
        return (
            <div className="my-2 ml-5">
                {/* <li className="nav-item">{this.props.name}</li>  */}
                {/* <li className="nav-item d-flex flex-row">
                    {this.props.balance}
                    <img src={uri} alt="coins" />
                </li> */}
                
                <div className="nav-item">{this.props.level + ' ' + this.props.room}</div>
            </div>
        )
    }
}
