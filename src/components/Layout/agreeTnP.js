import React, { Component } from 'react'

export default class Agree extends Component {

    constructor(){
        super();
        this.state = {
            isChecked : false,
        }
    }

    toggleCheckBox = () => {
        this.setState({isChecked: !this.state.isChecked}, ()=>this.props.onCheck())
    }

    render() {
        return (
            <div className="form-group mt-3">
                <div className="checkbox text-secondary">
                    <label className="small">
                        <input type="checkbox" checked={this.isChecked} onChange={this.toggleCheckBox} name="agree" value="agree"/>by clicking login you agree to our <a href="https://about.gloneglone.com/terms"><u>Terms of Service</u></a> and <a href="https://about.gloneglone.com/policy"><u>Privacy Policy</u></a>
                    </label>
                </div>
            </div>
        )
    }
}
