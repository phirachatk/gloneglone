import React, { Component } from 'react'

export default class ChatBox extends Component {
  render() {
    return (
      <div>
          <p>{this.props.msg.sender}</p>
          <p>{this.props.msg.message}</p>
      </div>
    )
  }
}
