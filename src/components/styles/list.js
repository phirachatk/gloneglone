import React from 'react';
import styled, {css} from 'styled-components';

const Button = styled.a`
    display: inline-block;
    padding: 15px;
    border-radius: 5px;
    background-color: #fff;
    color: #000;
    ${props => props.active === 'true' && css`
        background: red;
    `}
`;

const List = styled.ul`
    display: block;
`;

const ListItem = styled.li`
    display: block;
    border: 1px solid #ddd;
    background-color: #fff;
    color: #000;
`;

const ListGroup = props => {
    console.log(props, 'list props');
    const lists = props.list;
    return (
        <List>
            {lists.map(list => <ListItem>{list}</ListItem>)}
        </List>
    )
}

export { ListGroup, Button };