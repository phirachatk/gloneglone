import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'

import Login from './pages/Login.js'
import Main from './pages/Main'
import firebase from 'firebase'

import GameRoom from './pages/GameRoom';
import Redirect from './pages/Redirect.js';
import Write from './Write.js';

class App extends Component {
  constructor() {
    super();
    var config = {
      apiKey: "AIzaSyCA3-2QPY68NPvWoDj65x1LNhh4SCzDzQQ",
      authDomain: "gloneglone-game.firebaseapp.com",
      databaseURL: "https://gloneglone-game.firebaseio.com",
      projectId: "gloneglone-game",
      storageBucket: "gloneglone-game.appspot.com",
      messagingSenderId: "623448923017"
    };
    firebase.initializeApp(config);
  }

  render() {
    return (
      // <Write />
      <Switch>
        <Route path="/" component={Login} exact />
        <Route path="/main" component={Main} />
        <Route path="/game" component={GameRoom} exact />
        <Route component={Redirect} />
      </Switch>
    )
  }
}
export default App;
